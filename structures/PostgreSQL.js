const Sequelize = require('sequelize');
const winston = require('winston');

const database = new Sequelize('postgres://purge:s@localhost:5432/purgedb', { logging: false });

class Database {
  static get db() {
    return database;
  }

  static start() {
    database.authenticate()
      .then(() => console.info('[POSTGRES]: Connection to database has been established successfully.'))
      .then(() => console.info('[POSTGRES]: Synchronizing database...'))
      .then(() => database.sync()
        .then(() => console.info('[POSTGRES]: Done Synchronizing database!'))
        .catch(error => console.error(`[POSTGRES]: Error synchronizing the database: \n${error}`))
      )
      .catch(error => {
        console.error(`[POSTGRES]: Unable to connect to the database: \n${error}`);
        console.error(`[POSTGRES]: Try reconnecting in 5 seconds...`);
        setTimeout(() => Database.start(), 5000);
      });
  }
}

module.exports = Database;
