const Reminder = require('../../models/Reminder');
const moment = require('moment');

module.exports = class Reminders {
  static createReminder(userid, content, time, channelid) {
    return Reminder.create({
      userid: userid,
      content: content,
      time: time,
      channelid: channelid,
    });
  }

  static async deleteReminder(id, userid) {
    const reminder = await Reminder.findOne({
      where: {
        id: id,
        userid: userid,
      },
    });

    if (reminder) {
      reminder.destroy();
      return true;
    } else {
      return false;
    }
  }

  // eslint-disable-next-line no-empty-function
  static async isAuthor() {

  }

  static getReminder(id, userid) {
    return Reminder.findOne({
      where: {
        id: id,
        userid: userid,
      },
    });
  }

  static getReminders(user) {
    return Reminder.findAll({
      where: {
        userid: user,
      },
      order: [['time', 'ASC']],
    });
  }

  static async worker(client) {
    let reminders = await Reminder.findAll({
      limit: 10,
      order: [['time', 'ASC']],
    });

    reminders.forEach(async reminder => {
      if (reminder.time < Date.now()) {
        let userid = client.users.get(reminder.userid);
        let channel = client.channels.get(reminder.channelid);

        // Channel wont be found if it was a dm and the bot has restarted
        // this is a workaround for that as we have nothing else.
        if (!channel) channel = userid;

        console.log(`Reminder found. Intended for ${reminder.userid} in ${channel}.`);
        let color = channel.guild ? channel.guild.me.displayColor : 0xFFFFFF;

        // If we were really late, say sorry!
        if ((Date.now() - reminder.time) > 300000) {
          // ? let difference = Date.now() - reminder.time;
          let friendly = moment.unix(reminder.time / 1000).fromNow();
          reminder.destroy();
          await channel.send(userid, {
            embed: {
              color: color,
              title: 'Reminder!',
              description: `Sorry! I forgot to remind you about this ${friendly}.`,
              fields: [
                {
                  name: 'Reminder',
                  value: reminder.content,
                },
              ],
            },
          });
        }

        reminder.destroy();
        await channel.send(userid, {
          embed: {
            color: color,
            title: 'Reminder!',
            description: 'Here\'s that reminder you wanted!',
            fields: [
              {
                name: 'Reminder',
                value: reminder.content,
              },
            ],
          },
        });
      }

      return '';
    });

    // X setTimeout(() => Reminders.worker(client), 1000);
  }
};
