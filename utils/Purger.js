const moment = require('moment');
const { stripIndents } = require('common-tags');

module.exports = class Purger {
  static async worker(client) {
    let purgeActive = false;
    let purgeCount = 0;

    let nextPurge = client.provider.get('global', 'nextPurge', 0);

    if (nextPurge === 0) {
      return client.provider.set('global', 'nextPurge', moment().add(2, 'day').unix());
    } else if (nextPurge > moment().unix()) {
      return true;
    }

    let purgeStart = moment().unix();
    purgeActive = true;
    await client.provider.set('global', 'nextPurge', moment().add(2, 'day').unix());

    let guild = client.guilds.find(g => g.name === 'Blobs 3');
    let nsfwChannel = guild.channels.find(c => c.name === 'can-he-do-it');
    let reportChannel = guild.channels.find(c => c.name === 'general');
    let everyone = guild.roles.find(r => r.name === '@everyone');

    if (!guild.me.hasPermission('MANAGE_CHANNELS')) {
      return console.error('[ERROR]: Cannot continue, I do not have MANAGE_CHANNELS!');
    }

    if (!guild.me.hasPermission('MANAGE_MESSAGES')) {
      return console.error('[ERROR]: Cannot continue, I do not have MANAGE_MESSAGES!');
    }

    await nsfwChannel.overwritePermissions(everyone, { SEND_MESSAGES: false }).catch(error => {
      console.log(error);
    });

    while (purgeActive) {
      // eslint-disable-next-line no-await-in-loop
      await nsfwChannel.fetchMessages({ limit: 100 })
        .then(async messages => {
          if (messages.size > 0) {
            purgeCount += messages.size;
            await nsfwChannel.bulkDelete(messages);
          } else if (messages.size === 0) {
            /* eslint-disable max-len */
            await nsfwChannel.send(stripIndents`
              While this channel is labeled as NSFW, breaking Discord's Terms of Services (<https://discordapp.com/terms>) will not be allowed. This includes gore, child porn, animal abuse, revenge porn, etc.

              As a safety measure this channel will be purged regularly.

              Thanks.`).then(message => {
              message.pin();
              reportChannel.send(stripIndents`
                Purged \`${purgeCount}\` messages ${nsfwChannel}.
                \`Finished in ${moment.duration(moment().unix() - purgeStart, 's').format('h [hr] m [min] s [sec]')}\`
                `);

              purgeCount = 0;
              purgeActive = false;
            });
            /* eslint-enable max-len */
          }
        });
    }
    return nsfwChannel.overwritePermissions(everyone, { SEND_MESSAGES: true }).catch(console.error);
  }
};
