const Sequelize = require('sequelize');

const Database = require('../structures/PostgreSQL');

const Reminder = Database.db.define('reminder', {
  userid: Sequelize.STRING,
  content: Sequelize.STRING,
  time: Sequelize.BIGINT,
  channelid: Sequelize.STRING,
}, {
  timestamps: false,
});

module.exports = Reminder;
