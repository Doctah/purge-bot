const { oneLine, stripIndents } = require('common-tags');
const { Command } = require('discord.js-commando');
const { disambiguation } = require('discord.js-commando');

module.exports = class EnableCommandCommand extends Command {
  constructor(client) {
    super(client, {
      name: 'enable',
      aliases: ['enable-command', 'cmd-on', 'command-on'],
      group: 'core',
      memberName: 'enable',
      description: 'Enables a command or command group.',
      details: oneLine`
				The argument must be the name/ID (partial or whole) of a command or command group.
				Only administrators may use this command.
			`,
      examples: ['enable util', 'enable Utility', 'enable prefix'],
      guarded: true,

      args: [
        {
          key: 'cmdOrGrp',
          label: 'command/group',
          prompt: 'Which command or group would you like to enable?',
          validate: val => {
            if (!val) return false;
            const groups = this.client.registry.findGroups(val);
            if (groups.length === 1) return true;
            const commands = this.client.registry.findCommands(val);
            if (commands.length === 1) return true;
            if (commands.length === 0 && groups.length === 0) return false;
            return stripIndents`
							${commands.length > 1 ? disambiguation(commands, 'commands') : ''}
							${groups.length > 1 ? disambiguation(groups, 'groups') : ''}
						`;
          },
          parse: val => this.client.registry.findGroups(val)[0] || this.client.registry.findCommands(val)[0],
        },
      ],
    });
  }

  hasPermission(msg) {
    if (!msg.guild) return this.client.isOwner(msg.author);
    return msg.member.hasPermission('ADMINISTRATOR') || this.client.isOwner(msg.author);
  }

  run(msg, { cmdOrGrp }) {
    if (cmdOrGrp.isEnabledIn(msg.guild)) {
      return msg.reply(
        `The \`${cmdOrGrp.name}\` ${cmdOrGrp.group ? 'command' : 'group'} is already enabled.`
      );
    }
    cmdOrGrp.setEnabledIn(msg.guild, true);
    return msg.reply(`Enabled the \`${cmdOrGrp.name}\` ${cmdOrGrp.group ? 'command' : 'group'}.`);
  }
};
