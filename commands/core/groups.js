const { Command } = require('discord.js-commando');

module.exports = class ListGroupsCommand extends Command {
  constructor(client) {
    super(client, {
      name: 'groups',
      aliases: ['list-groups', 'show-groups'],
      group: 'core',
      memberName: 'groups',
      description: 'Lists all command groups.',
      details: 'Only administrators may use this command.',
      guarded: true,
    });
  }

  hasPermission(msg) {
    if (!msg.guild) return this.client.isOwner(msg.author);
    return msg.member.hasPermission('ADMINISTRATOR') || this.client.isOwner(msg.author);
  }

  run(msg) {
    let embed = {
      color: msg.guild ? msg.guild.me.displayColor : 0xFFFFFF,
      fields: [],
    };

    this.client.registry.groups.map(group => embed.fields.push({
      name: group.name,
      value: group.isEnabledIn(msg.guild) ? 'Enabled' : 'Disabled',
      inline: true,
    }));

    while (embed.fields.length % 3 !== 0) {
      embed.fields.push({
        name: '\u200B',
        value: '\u200B',
        inline: true,
      });
    }
    return msg.say({ embed });
  }
};
