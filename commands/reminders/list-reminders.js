const { Command, util } = require('discord.js-commando');
const { stripIndents, oneLine } = require('common-tags');
const moment = require('moment');
const Reminder = require('../../structures/reminders/Reminders');

module.exports = class ListRemindersCommand extends Command {
  constructor(client) {
    super(client, {
      name: 'list-reminders',
      group: 'reminders',
      aliases: ['reminders', 'reminder-list'],
      memberName: 'list-reminders',
      description: `Shows a list of your current reminders.`,
      args: [
        {
          key: 'page',
          prompt: 'what page would you like to view?\n',
          type: 'integer',
          default: 1,
        },
      ],
    });
  }

  async run(msg, { page }) {
    const reminders = await Reminder.getReminders(msg.author.id);
    const paginated = util.paginate(reminders, page, 10);

    let embed = {
      color: msg.guild ? msg.guild.me.displayColor : 0xFFFFFF,
      title: `Your reminders ${paginated.maxPage > 1 ? ` - Page ${paginated.page}/${paginated.maxPage}` : ''}`,
      description: stripIndents`
			Here is a list of your current reminders with their ID and expiration.

			To delete a reminder, use ${Command.usage('delete-reminder <id>', this.client.commandPrefix, this.client.user)}
			To view a reminder, use ${Command.usage('view-reminder <id>', this.client.commandPrefix, this.client.user)}

			${paginated.items.length === 0 ? 'No reminders to display!' :
    paginated.items.map(reminder => oneLine`**(${reminder.id})** 
    \`${moment.unix(reminder.time / 1000).format('ddd, MMM Do, YYYY HH:mm UTC')}\`
     (${moment.unix(reminder.time / 1000).fromNow(true)})`).join('\n')
}
			`,
    };

    return msg.reply({ embed });
  }
};
