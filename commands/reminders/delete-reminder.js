const { Command } = require('discord.js-commando');
const { stripIndents } = require('common-tags');
const Reminders = require('../../structures/reminders/Reminders');

module.exports = class DeleteReminderCommand extends Command {
  constructor(client) {
    super(client, {
      name: 'delete-reminder',
      group: 'reminders',
      memberName: 'delete-reminder',
      aliases: ['remove-reminder', 'del-reminder', 'rem-reminder'],
      description: `Deletes a reminder based on ID.`,
      args: [
        {
          key: 'id',
          prompt: 'what is the ID of the reminder you want to delete?',
          type: 'integer',
        },
      ],
    });
  }

  async run(msg, { id }) {
    let deletion = await Reminders.deleteReminder(id, msg.author.id);

    if (deletion) {
      return msg.reply('I have deleted that reminder for you.');
    } else if (!deletion) {
      return msg.reply(stripIndents`
			I failed to delete that reminder. Does it exist?
			View your reminders with \`${Command.usage('list-reminders')}\`
    `);
    } else {
      return msg.reply('Unknown error!');
    }
  }
};
