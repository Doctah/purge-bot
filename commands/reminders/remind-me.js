const { Command } = require('discord.js-commando');
const { stripIndents } = require('common-tags');
const Reminder = require('../../structures/reminders/Reminders');
const moment = require('moment');
const parse = require('parse-duration');

module.exports = class RemindMeCommand extends Command {
  constructor(client) {
    super(client, {
      name: 'remind-me',
      group: 'reminders',
      memberName: 'remind-me',
      description: `Reminds you to do something at a later period in time.`,
      details: stripIndents`
			Valid time formats are \`s or sec, m or min, h or hr, d, w or wk, month, y or yr\`.
			`,
      examples: ['remind-me 1h30m clean the dishes', 'remind-me 1d do your dailies!'],
      aliases: ['remind', 'set-reminder'],
      args: [
        {
          key: 'time',
          prompt: 'how long from now would you like to be reminded? Example: `1hr30min`\n',
          type: 'string',
          validate: time => {
            const parsed = parse(time);
            if (parsed === 0) return 'please provide a valid starting time. Example: `1hr30min`';
            return true;
          },
          parse: time => parse(time),
        },
        {
          key: 'reminder',
          prompt: 'what would you like to be reminded about?\n',
          type: 'string',
        },
      ],
    });
  }

  async run(msg, { time, reminder }) {
    let now = Date.now();
    let user = msg.author.id;
    let channel = msg.channel.id;

    Reminder.createReminder(user, reminder, now + time, channel);
    await msg.reply(`Sure, I'll remind you ${moment.unix((now + time) / 1000).fromNow()}.`);
  }
};
