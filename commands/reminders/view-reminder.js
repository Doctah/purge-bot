const { Command } = require('discord.js-commando');
const Reminders = require('../../structures/reminders/Reminders');
const moment = require('moment');
const { oneLine } = require('common-tags');

module.exports = class ViewReminderCommand extends Command {
  constructor(client) {
    super(client, {
      name: 'view-reminder',
      group: 'reminders',
      memberName: 'view-reminder',
      description: `Shows you the details of a reminder.`,
      aliases: ['reminder', 'reminder-info', 'info-reminder'],
      args: [
        {
          key: 'id',
          prompt: 'what is the ID of the reminder you want to view?',
          type: 'integer',
        },
      ],
    });
  }

  async run(msg, { id }) {
    const reminder = await Reminders.getReminder(id, msg.author.id);
    if (!reminder) return msg.reply('Sorry, you don\'t have a reminder with that ID.');
    let embed = {
      color: msg.guild ? msg.guild.me.displayColor : 0xFFFFFF,
      fields: [
        {
          name: 'Reminder ID',
          value: `${reminder.id}`,
        },
        {
          name: 'Content',
          value: `${reminder.content}`,
        },
        {
          name: 'Expiration',
          value: oneLine`
          ${moment.unix(reminder.time / 1000).format('ddd, MMM Do, YYYY HH:mm UTC')}
          (${moment.unix(reminder.time / 1000).fromNow(true)})
          `,
        },
      ],
    };

    return msg.reply({ embed });
  }
};
