const { Command } = require('discord.js-commando');
const moment = require('moment');
const { stripIndents } = require('common-tags');

const humanLevels = {
  0: 'None',
  1: 'Low',
  2: 'Medium',
  3: '(╯°□°）╯︵ ┻━┻',
  4: '┻━┻ ﾐヽ(ಠ益ಠ)ノ彡┻━┻',
};

module.exports = class ServerInfoCommand extends Command {
  constructor(client) {
    super(client, {
      name: 'server-info',
      aliases: ['server'],
      group: 'info',
      memberName: 'server',
      description: 'Get info on the server.',
      details: `Get detailed information on the server.`,
      guildOnly: true,
      throttling: {
        usages: 2,
        duration: 3,
      },
    });
  }

  run(msg) {
    return msg.embed({
      color: msg.guild ? msg.guild.me.displayColor : 0xFFFFFF,
      author: {
        name: msg.guild.name,
        icon_url: msg.guild.iconURL,

      },
      fields: [
        {
          name: 'Channels',
          /* eslint-disable max-len */
          value: stripIndents`
						• ${msg.guild.channels.filter(ch => ch.type === 'text').size} Text, ${msg.guild.channels.filter(ch => ch.type === 'voice').size} Voice
						• Default: ${msg.guild.defaultChannel}
						• AFK: ${msg.guild.afkChannelID ? `<#${msg.guild.afkChannelID}> after ${msg.guild.afkTimeout / 60}min` : 'None'}
					`,
          /* eslint-enable max-len */
          inline: true,
        },
        {
          name: 'Members',
          value: stripIndents`
						• ${msg.guild.presences.filter(p => p.status !== 'offline').size} / ${msg.guild.memberCount} members online
						• Owner: ${msg.guild.owner}
					`,
          inline: true,
        },
        {
          name: 'Other',
          value: stripIndents`
						• Roles: ${msg.guild.roles.size}
						• Region: ${msg.guild.region}
						• Created at: ${moment.utc(msg.guild.createdAt).format('dddd, MMMM Do YYYY, HH:mm:ss ZZ')}
						• Verification Level: ${humanLevels[msg.guild.verificationLevel]}
					`,
        },
      ],
      thumbnail: { url: msg.guild.iconURL },
    });
  }
};
