const { Command } = require('discord.js-commando');
const { exec } = require('child_process');

module.exports = class RestartCommand extends Command {
  constructor(client) {
    super(client, {
      name: 'restart',
      group: 'util',
      memberName: 'restart',
      description: 'Restarts the bot.',
    });
  }

  hasPermission(msg) {
    return this.client.isOwner(msg.author);
  }

  async run(msg) {
    await msg.channel.send('Restarting in 3 seconds...').then(() => {
      setTimeout(() => exec('pm2 restart 0'));
    });
  }
};
