/* eslint-disable max-len */
const { stripIndents, oneLine } = require('common-tags');
const { Command } = require('discord.js-commando');
const disambiguation = require('../../utils/Disambiguation');
const Discord = require('discord.js');

module.exports = class HelpCommand extends Command {
  constructor(client) {
    super(client, {
      name: 'help',
      group: 'util',
      memberName: 'help',
      description: 'Displays a list of available commands, or detailed information for a specified command.',
      details: oneLine`
				The command may be part of a command name or a whole command name.
				If it isn't specified, all available commands will be listed.
			`,
      examples: ['help', 'help prefix'],
      guarded: true,

      args: [
        {
          key: 'command',
          prompt: 'Which command would you like to view the help for?\n',
          type: 'string',
          default: '',
        },
      ],
    });
  }

  run(msg, args) { // eslint-disable-line complexity
    const groups = this.client.registry.groups;
    const commands = this.client.registry.findCommands(args.command, false, msg);
    const showAll = args.command && args.command.toLowerCase() === 'all';
    let embed = new Discord.RichEmbed;

    if (args.command && !showAll) {
      if (commands.length === 0) {
        return msg.reply(
          `Unable to identify command. Use ${msg.usage(
            null, msg.channel.type === 'dm' ? null : undefined, msg.channel.type === 'dm' ? null : undefined
          )} to view the list of all commands.`
        );
      } else if (commands.length > 1) {
        return msg.reply(disambiguation(commands, 'commands'));
      } else if (commands.length > 15) {
        return msg.reply('Multiple commands found. Please be more specific.');
      }
      embed.setAuthor(commands[0].name);
      embed.setDescription(commands[0].description);

      if (commands[0].aliases) embed.addField('Aliases', `\`${commands[0].aliases.join('`, `')}\``);
      if (commands[0].details) embed.addField('Details', commands[0].details);
      if (commands[0].examples) embed.addField('Examples', `\`${commands[0].examples.join('`\n `')}\``);

      return msg.author.send('', { embed })
        .then(() => {
          if (msg.guild) msg.reply('Check your DMs for more information.');
        })
        .catch(() => {
          msg.reply('There was an error sending the help DM. Do you have DMs disabled?');
        });
    } else if (!showAll) {
      embed.setAuthor(`Usable commands in ${msg.channel.type === 'dm' ? 'this DM' : msg.guild.name}`);
      embed.setDescription(stripIndents`
      To run a command in ${msg.guild || 'any server'} use ${Command.usage('command', msg.guild ? msg.guild.commandPrefix : null, this.client.user)}
      For example, ${Command.usage('prefix', msg.guild ? msg.guild.commandPrefix : null, this.client.user)}.

      To learn more about a command, use ${Command.usage('help <command>', msg.guild ? msg.guild.commandPrefix : null, this.client.user)}.
      `);
      groups
        .filter(grp => grp.commands.some(cmd => cmd.isUsable(msg)))
        .map(grp => embed.addField(grp.name, `\`${grp.commands.map(cmd => cmd.name).join('`, `')}\``, false));

      return msg.author.send('', { embed })
        .then(() => {
          if (msg.guild) msg.reply('Check your DMs for more information.');
        })
        .catch(() => {
          msg.reply('There was an error sending the help DM. Do you have DMs disabled?');
        });
    } else if (showAll) {
      embed.setAuthor(`All commands in ${msg.channel.type === 'dm' ? 'this DM' : msg.guild.name}`);
      embed.setDescription(stripIndents`
      To run a command in ${msg.guild || 'any server'} use ${Command.usage('command', msg.guild ? msg.guild.commandPrefix : null, this.client.user)}
      For example, ${Command.usage('prefix', msg.guild ? msg.guild.commandPrefix : null, this.client.user)}.

      To learn more about a command, use ${Command.usage('help <command>', msg.guild ? msg.guild.commandPrefix : null, this.client.user)}.
      `);
      groups.map(grp => embed.addField(grp.name, `\`${grp.commands.map(cmd => cmd.name).join('`, `')}\``));

      return msg.author.send('', { embed })
        .then(() => {
          if (msg.guild) msg.reply('Check your DMs for more information.');
        })
        .catch(() => {
          msg.reply('There was an error sending the help DM. Do you have DMs disabled?');
        });
    } else {
      return null;
    }
  }
};
