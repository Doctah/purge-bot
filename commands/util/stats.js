const { Command } = require('discord.js-commando');
const moment = require('moment');
require('moment-duration-format');
const { stripIndents } = require('common-tags');

module.exports = class StatsCommand extends Command {
  constructor(client) {
    super(client, {
      name: 'stats',
      aliases: ['statistics'],
      group: 'util',
      memberName: 'stats',
      description: 'Displays statistics about the bot.',
      guildOnly: true,
      throttling: {
        usages: 2,
        duration: 3,
      },
    });
  }

  run(msg) {
    return msg.embed({
      color: msg.guild ? msg.guild.me.displayColor : 0xFFFFFF,
      author: {
        name: 'Statistics',
        icon_url: this.client.user.avatarURL,
      },
      fields: [
        {
          name: 'Uptime',
          value: moment.duration(this.client.uptime)
            .format('d[ days], h[ hours], m[ minutes, and ]s[ seconds]'),
          inline: false,
        },
        {
          name: 'Memory usage',
          value: `${Math.round(process.memoryUsage().heapUsed / 1024 / 1024)}MB`,
          inline: false,
        },
        {
          name: 'General Stats',
          value: stripIndents`
					• Guilds: ${this.client.guilds.size}
					• Channels: ${this.client.channels.size}
					• Users: ${this.client.guilds.map(guild => guild.memberCount).reduce((a, b) => a + b)}
					`,
          inline: false,
        },
      ],
    });
  }
};
