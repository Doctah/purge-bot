const { Command } = require('discord.js-commando');
const { exec } = require('child_process');

module.exports = class ExecCommand extends Command {
  constructor(client) {
    super(client, {
      name: 'exec',
      group: 'util',
      memberName: 'exec',
      description: 'Executes code on the machine the bot is on.',
      args: [
        {
          key: 'command',
          prompt: '',
          type: 'string',
        },
      ],
    });
  }

  hasPermission(msg) {
    return this.client.isOwner(msg.author);
  }

  async run(msg, args) {
    await exec(args.command, (error, stdout) => {
      if (error) {
        console.error(`[COMMAND]: ${error}`);
        msg.reply('Error! Check console output.');
      }
      msg.channel.send(stdout, { code: true });
    });
  }
};
