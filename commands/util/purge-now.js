const { Command } = require('discord.js-commando');
const moment = require('moment');

module.exports = class PurgeNowCommand extends Command {
  constructor(client) {
    super(client, {
      name: 'purge-now',
      group: 'util',
      memberName: 'purge-now',
      description: 'Executes code on the machine the bot is on.',
    });
  }

  hasPermission(msg) {
    return this.client.isOwner(msg.author) || msg.member.hasPermission('MANAGE_MESSAGES');
  }

  async run(msg) {
    await this.client.provider.set('global', 'nextPurge', moment().unix());
    await msg.reply('Purge will begin shortly.');
  }
};
