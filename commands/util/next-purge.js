const { Command } = require('discord.js-commando');
const moment = require('moment');
require('moment-duration-format');
const { oneLine } = require('common-tags');

module.exports = class TestCommand extends Command {
  constructor(client) {
    super(client, {
      name: 'next-purge',
      group: 'util',
      memberName: 'next-purge',
      description: `Displays when the next message purge will happen.`,
    });
  }

  hasPermission(msg) {
    return this.client.isOwner(msg.author) || msg.member.hasPermission('MANAGE_MESSAGES');
  }

  async run(msg) {
    let nextPurge = this.client.provider.get('global', 'nextPurge', 0);

    await msg.reply(oneLine`
    The next purge will occur at ${moment.unix(nextPurge).utc().format('MMMM Do, h:mm A Z')}
    (${moment.duration(nextPurge - moment().unix(), 's').format('h [hr] m [min] s [sec]')} from now).
    `);
  }
};
