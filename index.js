const { FriendlyError } = require('discord.js-commando');
const path = require('path');
const { oneLine } = require('common-tags');
const SequelizeProvider = require('./providers/Sequelize');
const CommandoClient = require('./structures/CommandoClient');

const { OWNER, PREFIX, TOKEN } = require('./config.json');
const Reminders = require('./structures/reminders/Reminders');
const userName = require('./models/UserName');
const Purger = require('./utils/Purger');

const client = new CommandoClient({
  owner: OWNER,
  commandPrefix: PREFIX,
  unknownCommandResponse: false,
  disableEveryone: true,
});

client.setProvider(new SequelizeProvider(client.database));

client.dispatcher.addInhibitor(msg => {
  const blacklist = client.provider.get('global', 'userBlacklist', []);
  if (!blacklist.includes(msg.author.id)) return false;
  return `Has been blacklisted.`;
});


client.on('error', console.error)
  .on('warn', console.log)
  .once('ready', () => {
    setInterval(() => Reminders.worker(client), 1000);
    setInterval(() => Purger.worker(client), 10000);
  })
  .on('ready', () => {
    console.log(`[DISCORD]: Client ready... Logged in as ${client.user.tag}`);
  })
  .on('disconnect', () => console.log('[DISCORD]: Disconnected!'))
  .on('reconnect', () => console.log('[DISCORD]: Reconnecting...'))
  .on('commandRun', (cmd, promise, msg, args) =>
    console.log(oneLine`
			[DISCORD]: ${msg.author.tag}
			> ${msg.guild ? `${msg.guild.name}` : 'DM'}
			>> ${cmd.groupID}:${cmd.memberName}
			${Object.values(args).length > 1 ? `>>> ${Object.values(args)}` : ''}
		`)
  )
  .on('unknownCommand', msg => {
    if (msg.channel.type === 'dm') return;
    if (msg.author.bot) return;

    const args = { name: msg.content.split(client.commandPrefix)[1].toLowerCase() };
    client.registry.resolveCommand('tags:tag').run(msg, args);
  })
  .on('commandError', (cmd, err) => {
    if (err instanceof FriendlyError) return;
    console.log(`[DISCORD]: Error in command ${cmd.groupID}:${cmd.memberName}`, err);
  })
  .on('commandBlocked', (msg, reason) => {
    console.log(oneLine`
			[DISCORD]: Command ${msg.command ? `${msg.command.groupID}:${msg.command.memberName}` : ''}
			blocked; User ${msg.author.tag}: ${reason}
		`);
  })
  .on('commandPrefixChange', (guild, prefix) => {
    console.log(oneLine`
			[DISCORD]: Prefix changed to ${prefix || 'the default'}
			${guild ? `in guild ${guild.name}` : 'globally'}.
		`);
  })
  .on('commandStatusChange', (guild, command, enabled) => {
    console.log(oneLine`
			[DISCORD]: Command ${command.groupID}:${command.memberName}
			${enabled ? 'enabled' : 'disabled'}
			${guild ? `in guild ${guild.name}` : 'globally'}.
		`);
  })
  .on('groupStatusChange', (guild, group, enabled) => {
    console.log(oneLine`
			[DISCORD]: Group ${group.id}
			${enabled ? 'enabled' : 'disabled'}
			${guild ? `in guild ${guild.name} (${guild.id})` : 'globally'}.
		`);
  })
  .on('providerReady', () => {
    console.log(`[DISCORD]: Provider ready!`);
  })
  .on('userUpdate', (oldUser, newUser) => {
    if (oldUser.username !== newUser.username) {
      userName.create({ userID: newUser.id, username: oldUser.username }).catch(console.error);
    }
  });

client.registry
  .registerGroups([
    ['util', 'Utility'],
    ['core', 'Core'],
    ['info', 'Information'],
    ['reminders', 'Reminders'],
  ])
  .registerDefaultTypes()
  .registerDefaultCommands({
    help: false,
    prefix: false,
    ping: false,
    eval_: false,
    commandState: false,
    unknownCommand: false,
  })
  .registerCommandsIn(path.join(__dirname, 'commands'));

client.login(TOKEN);
